class cinder_nfs::setconf (
  $service_scheduler = 'cinder-scheduler',
  $service_api       = 'cinder-api',
){

cinder_config {
    "DEFAULT/scheduler_driver":    value => 'cinder.scheduler.filter_scheduler.FilterScheduler';
    "DEFAULT/default_volume_type": value => 'volumes_nfs';
  }

  service { $service_scheduler :
    ensure => running,
  }

  service { $service_api :
    ensure => running,
  }

}
