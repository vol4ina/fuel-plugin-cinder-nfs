class cinder_nfs::hiera-override {
	# resources
  $cinder_nfs_hash    = hiera_hash('fuel-plugin-cinder-nfs',{})
  $hiera_dir = '/etc/hiera/plugins'
  $plugin_name = 'fuel-plugin-cinder-nfs'
  $plugin_yaml = "${plugin_name}.yaml"
  $content = "storage:
      volume_backend_names:
        volumes_nfs: NFS-backend
        volumes_lvm: LVM-backend"

  file {$hiera_dir:
    ensure  => directory,
  } ->
  file {"delete-${hiera_dir}/${plugin_yaml}":
    ensure  => absent,
    path    => "${hiera_dir}/${plugin_yaml}",
  }->
  file { "${hiera_dir}/${plugin_yaml}":
    ensure  => file,
    content => $content,
    require => File['/etc/hiera/plugins']
  }
}