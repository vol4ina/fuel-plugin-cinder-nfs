class cinder_nfs::required (

  $required    = $::cinder_nfs::params::required_packages,
  $cinder_pack = $::cinder_nfs::params::cinder_package,
  $vol_service = $::cinder_nfs::params::cinder_service_name,
)

{

  package { $required:
    ensure => present,
  }

  package { 'cinder':
    name   => $cinder_pack,
    ensure => present,
  }
  package { 'cinder-volume':
    name   => $vol_service,
    ensure => present,
  }~>
  service { $vol_service:
    ensure => running,
  }
}

