class cinder_nfs::params {
  $cinder_nfs_share_config = '/etc/cinder/nfs_shares'
  $cinder_nfs_config       = '/etc/cinder/cinder.conf'

  if $::osfamily == 'Debian' {
    $required_packages      = [ 'nfs-common' ]
    $package_name           = [ 'nfs-common' ]
    $cinder_package         = [ 'cinder-common' ]

    $cinder_bk_service_name = 'cinder-backup'
    $cinder_service_name    = 'cinder-volume'

  }else {
    fail("unsuported osfamily ${::osfamily}, currently Debian and Redhat are the only supported platforms")
  }
}