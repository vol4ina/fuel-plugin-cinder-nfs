notice('PLUGIN fuel-plugin-cinder-nfs: site.pp')

include cinder_nfs::params
include cinder_nfs::required

$cinder_nfs_hash	      = hiera_hash('fuel-plugin-cinder-nfs',{})
$nfs_mount_options      = $cinder_nfs_hash['nfs_mount_options']
$nfs_share              = split($cinder_nfs_hash['nfs_share'],',')
# Pull Required Data From Hiera
$cinder_volume_group    = hiera('cinder_volume_group', 'cinder')
$storage_hash           = hiera_hash('storage', {})
$nfs_shares_config      = $cinder_nfs::params::cinder_nfs_share_config
$volume_backend_name    = $storage_hash['volume_backend_names']['volumes_nfs']

$backends = $storage_hash['volume_backend_names']
if $backends['volumes_ceph'] {
  fail('ceph backend currently is not supported')
}

$available_backends  = delete_values($backends, false)

cinder_config {
  "DEFAULT/enabled_backends": value => join(values($available_backends),',');
  "DEFAULT/default_volume_type": value => $volume_backend_name;
}


cinder::backend::nfs {$volume_backend_name:
  nfs_servers          => $nfs_share,
  nfs_mount_options    => $nfs_mount_options,
  nfs_shares_config    => $nfs_shares_config,
  volume_backend_name  => $volume_backend_name,
}